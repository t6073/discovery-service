FROM openjdk:11
EXPOSE 8761
WORKDIR .
COPY . .
CMD ["java", "-jar","./target/discovery-0.0.1-SNAPSHOT.jar"]